#Set this to @ to keep the makefile quiet
ifndef SILENCE
	SILENCE =
endif

# Y/N
ifndef DEBUG
	DEBUG=N
endif

#Clear compiler and linker flags for test code
ifndef PRODUCTION_ONLY
	PRODUCTION_ONLY = N
endif

### Directory structure and library list ###
TARGET_NAME=I2cLedDemo
TARGET_DIR=build
TARGET=$(TARGET_DIR)/$(TARGET_NAME)

# Production code
SRC_DIRS=src
INC_DIRS=inc
LIB_DIRS=lib
#Static library names without lib prefix and .a suffix
LIB_LIST=
OBJ_DIR=obj

# Chip-specific production code
MCU_SRC_DIR=LPC1768src
MCU_INC_DIR=LPC1768src ../CMSIS_CORE_LPC17xx/inc
MCU_LIB_DIRS=../CMSIS_CORE_LPC17xx
MCU_LIB_LIST=CMSIS_CORE_LPC17xx

# CppUTest source
CPPUTEST_DIR=cpputest-3.6
CPPUTEST_HOME=../../../$(CPPUTEST_DIR)
CPPUTEST_INC_DIR=$(CPPUTEST_HOME)/include
CPPUTEST_LIB_DIR=$(CPPUTEST_HOME)/lib
CPPUTEST_LIB_LIST=CppUTest CppUTestExt

# Test code
TEST_DIR=test
TEST_SRC_DIRS=$(TEST_DIR)/src $(TEST_DIR)/mocks
TEST_INC_DIR=$(TEST_DIR)/inc
TEST_LIB_DIRS=$(TEST_DIR)/lib
#Static library names without lib prefix and .a suffix
TEST_LIB_LIST=
TEST_TARGET_DIR=$(TEST_DIR)/build
TEST_OBJ_DIR=$(TEST_DIR)/$(OBJ_DIR)

# Debug code
#Too similar to LPCXpresso's Debug folder?
DEBUG_DIR=debug

### Compiler tools ###
COMPILER=g++
LINKER=g++
ARCHIVER=ar

include MakefileWorker.make
