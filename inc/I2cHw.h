#ifndef D_I2cHw_H
#define D_I2cHw_H

#ifdef __cplusplus
extern "C" {
#endif

typedef enum
{
  SCL = 1,
  SDA = 2
} I2cLine;

typedef enum
{
  LOW  = 0,
  HIGH = 1
} I2cLineState;

void I2cHw_Init(void);
I2cLineState I2cHw_CheckLineState(I2cLine line);
void I2cHw_PullI2cLineLow(I2cLine line);
void I2cHw_ReleaseI2cLine(I2cLine line);

#ifdef __cplusplus
}
#endif

#endif /*D_I2cHw_H*/
