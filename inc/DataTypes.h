#ifndef D_DataTypes_H
#define D_DataTypes_H

#ifdef __cplusplus
extern "C" {
#endif

typedef unsigned char  u08;
typedef   signed char  s08;
typedef unsigned short u16;
typedef   signed short s16;
typedef unsigned long  u32;
typedef   signed long  s32;

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#ifndef BOOL
#define BOOL int
#endif

#ifdef __cplusplus
}
#endif

#endif  // D_DataTypes_H

