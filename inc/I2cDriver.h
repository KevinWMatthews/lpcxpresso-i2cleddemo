#ifndef D_I2cDriver_H
#define D_I2cDriver_H

#ifdef __cplusplus
extern "C" {
#endif

#include "DataTypes.h"

typedef enum
{
  ZERO = 0,
  ONE  = 1
} I2cData;

s08 I2cDriver_ClockStart(void);
void I2cDriver_Init(void);
s08 I2cDriver_SendData(I2cData data);
s08 I2cDriver_SendByte(int byte);
s08 I2cDriver_Start(void);
s08 I2cDriver_Stop(void);

typedef enum
{
  I2C_SUCCESS = 0,
  I2C_ERR_INVALID_LINE_STATE = -1
} I2cErrorCodes;

#ifdef __cplusplus
}
#endif

#endif /*D_I2cDriver_H*/
