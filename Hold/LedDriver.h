#ifndef D_LedDriver_H
#define D_LedDriver_H

#ifdef __cplusplus
extern "C" {
#endif

#include "DataTypes.h"

typedef enum
{
  LED1 = 1,
  LED2 = 2,
  LED3 = 3,
  LED4 = 4
} LedNum;

// Function prototypes
void LedDriver_Init(void);
void LedDriver_AllOff(void);
void LedDriver_AllOn(void);
BOOL LedDriver_IsOn(LedNum ledNum);
void LedDriver_Invert(void);
void LedDriver_Off(LedNum ledNum);
void LedDriver_On(LedNum ledNum);

#ifdef __cplusplus
}
#endif

#endif /*D_LedDriver_H*/
