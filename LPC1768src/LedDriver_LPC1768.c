#include "LedDriver.h"
#include "LPC17xx.h"
#include "DataTypes.h"

// This code implements the interface found in LedDriver.h
// It is specific to the Atmel LPC1768 chip on the RDB1768 development board.
// To use a different chip, replace this file with a different implementation.

// Set of RDB1768 board user leds
#define LED_2 (1 << 24)
#define LED_3 (1 << 25)
#define LED_4 (1 << 28)
#define LED_5 (1 << 29)
// All Leds
#define LED_ALL (LED_2 | LED_3 | LED_4 | LED_5)

static u32 convertDriverLedToHwLed(LedNum ledNum);

// Function to initialise GPIO to access the user LEDs
void LedDriver_Init(void)
{
  // Set P1_24, P1_25, P1_28, P1_29 to 00 - GPIO
  LPC_PINCON->PINSEL3 &= (~0x0F0F0000);
  // Set GPIO - P1_24, P1_25, P1_28, P1_29 - to be outputs
  LPC_GPIO1->FIODIR |= LED_ALL;
}

// Function to turn all leds off
void LedDriver_AllOff(void)
{
  LPC_GPIO1->FIOCLR = LED_ALL;
}

// Function to turn all leds on
void LedDriver_AllOn(void)
{
  LPC_GPIO1->FIOSET = LED_ALL;
}

// Function to invert current state of leds
void LedDriver_Invert(void)
{
  int allLedStates;

  // Read current state of GPIO P1_16..31, which includes LEDs
  allLedStates = LPC_GPIO1->FIOPIN;
  // Turn off LEDs that are on
  // (ANDing to ensure we only affect the LED outputs)
  LPC_GPIO1->FIOCLR = allLedStates & LED_ALL;
  // Turn on LEDs that are off
  // (ANDing to ensure we only affect the LED outputs)
  LPC_GPIO1->FIOSET = ((~allLedStates) & LED_ALL);
}

// Function to turn off chosen led(s)
void LedDriver_Off(LedNum ledNum)
{
  // Turn off requested LEDs
  // (ANDing to ensure we only affect the LED outputs)
  LPC_GPIO1->FIOCLR = convertDriverLedToHwLed(ledNum) & LED_ALL;
}

// Function to turn on chosen led(s)
void LedDriver_On(LedNum ledNum)
{
  // Turn on requested LEDs
  // (ANDing to ensure we only affect the LED outputs)
  LPC_GPIO1->FIOSET = convertDriverLedToHwLed(ledNum) & LED_ALL;
}

//*** Internal functions ***//
// Change driver's general LED number references to HW-specific bit references
static u32 convertDriverLedToHwLed(LedNum ledNum)
{
  switch (ledNum)
  {
  case LED1:
    return LED_3;
  case LED2:
    return LED_2;
  case LED3:
    return LED_5;
  case LED4:
    return LED_4;
  default:
    return 0;
  }
  return 0;
}
