#ifndef D_LedDriverSpy_H
#define D_LedDriverSpy_H

#ifdef __cplusplus
extern "C" {
#endif

#include "LedDriver.h"
#include "DataTypes.h"

#define ALL_OFF 0x0
#define ALL_ON  0xf

u08 LedDriverSpy_AllLeds(void);
BOOL LedDriverSpy_PatternMatch(u08 ledPattern);

#ifdef __cplusplus
}
#endif

#endif /*D_LedDriverSpy_H*/
