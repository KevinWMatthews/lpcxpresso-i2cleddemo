#ifndef D_I2cLedSpy_H
#define D_I2cLedSpy_H

#ifdef __cplusplus
extern "C" {
#endif

#include "I2cLed.h"

void I2cLedSpy_SetSda(I2cLineState state);
void I2cLedSpy_SetScl(I2cLineState state);

#ifdef __cplusplus
}
#endif

#endif /*D_I2cLedSpy_H*/
