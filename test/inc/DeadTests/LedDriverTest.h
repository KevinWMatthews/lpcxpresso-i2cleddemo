#ifndef D_LedDriverSpyTest_h
#define D_LedDriverSpyTest_h

class LedDriverSpyTest
{
  public:
    explicit LedDriverSpyTest();
    virtual ~LedDriverSpyTest();

  private:
    LedDriverSpyTest(const LedDriverSpyTest&);
    LedDriverSpyTest& operator=(const LedDriverSpyTest&);
};

#endif  // D_LedDriverSpyTest_h
