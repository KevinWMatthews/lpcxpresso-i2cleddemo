#ifndef D_I2cLedTest_h
#define D_I2cLedTest_h

class I2cLedTest
{
  public:
    explicit I2cLedTest();
    virtual ~I2cLedTest();

  private:
    I2cLedTest(const I2cLedTest&);
    I2cLedTest& operator=(const I2cLedTest&);
};

#endif  // D_I2cLedTest_h
