#ifndef D_SampleTestGroup_h
#define D_SampleTestGroup_h

class SampleTestGroup
{
  public:
    explicit SampleTestGroup();
    virtual ~SampleTestGroup();

  private:
    SampleTestGroup(const SampleTestGroup&);
    SampleTestGroup& operator=(const SampleTestGroup&);
};

#endif  // D_SampleTestGroup_h
