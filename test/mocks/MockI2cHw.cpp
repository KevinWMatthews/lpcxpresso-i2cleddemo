extern "C"
{
  #include "I2cDriver.h"
  #include "I2cHw.h"
}

#include "CppUTest/TestHarness.h"
#include "CppUTestExt/MockSupport.h"

// Dead drop for simulating I2C lines
I2cLineState scl;
I2cLineState sda;

TEST_GROUP(MockI2cHw)
{
  void setup()
  {
    sda = HIGH;
    scl = HIGH;
  }

  void teardown()
  {
    mock().clear();
  }
};

// TEST(MockI2cHw, WiringTest)
// {
//   FAIL("Wiring test for MockI2cHw!");
// }

//*** Mock ~ALL~ production code functions in the file that you are testing ***/
void I2cHw_Init(void)
{}

I2cLineState I2cHw_CheckLineState(I2cLine line)
{
  mock().actualCall("I2cHw_CheckLineState").withParameter("p1",line);
  if (line == SCL)
    return (I2cLineState)mock().intReturnValue();
  if (line == SDA)
    return (I2cLineState)mock().intReturnValue();
}

void I2cHw_PullI2cLineLow(I2cLine line)
{
  mock().actualCall("I2cHw_PullI2cLineLow").withParameter("p1",line);
  if (line == SCL)
    scl = LOW;
  if (line == SDA)
    sda = LOW;
}

void I2cHw_ReleaseI2cLine(I2cLine line)
{
  mock().actualCall("I2cHw_ReleaseI2cLine").withParameter("p1",line);
  if (line == SCL)
    scl = HIGH;
  if (line == SDA)
    sda = HIGH;
}


//*** Tests ***//
TEST(MockI2cHw, StartFailsSclLow)
{
  scl = LOW;
  mock().expectOneCall("I2cHw_CheckLineState").withParameter("p1",SCL).andReturnValue(scl);
  LONGS_EQUAL(I2C_ERR_INVALID_LINE_STATE, I2cDriver_Start());
  LONGS_EQUAL(LOW, scl);
  mock().checkExpectations();
}

TEST(MockI2cHw, StartFailsSdaLow)
{
  scl = HIGH;
  sda = LOW;
  mock().expectOneCall("I2cHw_CheckLineState").withParameter("p1",SCL).andReturnValue(scl);
  mock().expectOneCall("I2cHw_CheckLineState").withParameter("p1",SDA).andReturnValue(sda);
  LONGS_EQUAL(I2C_ERR_INVALID_LINE_STATE, I2cDriver_Start());
  LONGS_EQUAL(HIGH, scl);
  LONGS_EQUAL(LOW, sda);
  mock().checkExpectations();
}

TEST(MockI2cHw, StartSucceeds)
{
  mock().expectOneCall("I2cHw_CheckLineState").withParameter("p1",SCL).andReturnValue(scl);
  mock().expectOneCall("I2cHw_CheckLineState").withParameter("p1",SDA).andReturnValue(sda);
  mock().expectOneCall("I2cHw_PullI2cLineLow").withParameter("p1",SDA);
  LONGS_EQUAL(I2C_SUCCESS, I2cDriver_Start());
  LONGS_EQUAL(HIGH, scl);
  LONGS_EQUAL(LOW, sda);
  mock().checkExpectations();
}

TEST(MockI2cHw, StopFailsSclLow)
{
  scl = LOW;
  mock().expectOneCall("I2cHw_CheckLineState").withParameter("p1",SCL).andReturnValue(scl);
  LONGS_EQUAL(I2C_ERR_INVALID_LINE_STATE, I2cDriver_Stop());
  LONGS_EQUAL(LOW, scl);
  mock().checkExpectations();
}

TEST(MockI2cHw, StopFailsSdaHigh)
{
  scl = HIGH;
  sda = HIGH;
  mock().expectOneCall("I2cHw_CheckLineState").withParameter("p1",SCL).andReturnValue(scl);
  mock().expectOneCall("I2cHw_CheckLineState").withParameter("p1",SDA).andReturnValue(sda);
  LONGS_EQUAL(I2C_ERR_INVALID_LINE_STATE, I2cDriver_Stop());
  LONGS_EQUAL(HIGH, scl);
  LONGS_EQUAL(HIGH, sda);
  mock().checkExpectations();
}

TEST(MockI2cHw, StopSucceeds)
{
  scl = HIGH;
  sda = LOW;
  mock().expectOneCall("I2cHw_CheckLineState").withParameter("p1",SCL).andReturnValue(scl);
  mock().expectOneCall("I2cHw_CheckLineState").withParameter("p1",SDA).andReturnValue(sda);
  mock().expectOneCall("I2cHw_ReleaseI2cLine").withParameter("p1",SDA);
  LONGS_EQUAL(I2C_SUCCESS, I2cDriver_Stop());
  LONGS_EQUAL(HIGH, scl);
  LONGS_EQUAL(HIGH, sda);
  mock().checkExpectations();
}

TEST(MockI2cHw, ClockStartFailsIfSclLow)
{
  scl = LOW;
  mock().expectOneCall("I2cHw_CheckLineState").withParameter("p1",SCL).andReturnValue(scl);
  LONGS_EQUAL(I2C_ERR_INVALID_LINE_STATE,I2cDriver_ClockStart());
  mock().checkExpectations();
}

TEST(MockI2cHw, ClockStartSucceeds)
{
  scl = HIGH;
  mock().expectOneCall("I2cHw_CheckLineState").withParameter("p1",SCL).andReturnValue(scl);
  mock().expectOneCall("I2cHw_PullI2cLineLow").withParameter("p1",SCL);
  LONGS_EQUAL(I2C_SUCCESS,I2cDriver_ClockStart());
  LONGS_EQUAL(LOW, scl);
  mock().checkExpectations();
}

TEST(MockI2cHw, SendData_FailsIfSclHigh)
{
  scl = HIGH;
  mock().expectOneCall("I2cHw_CheckLineState").withParameter("p1",SCL).andReturnValue(scl);
  LONGS_EQUAL(I2C_ERR_INVALID_LINE_STATE,I2cDriver_SendData(ZERO));
  LONGS_EQUAL(HIGH, scl);
  mock().checkExpectations();
}

TEST(MockI2cHw, SendData_Zero)
{
  scl = LOW;
  mock().expectOneCall("I2cHw_CheckLineState").withParameter("p1",SCL).andReturnValue(scl);
  mock().expectOneCall("I2cHw_PullI2cLineLow").withParameter("p1",SDA);
  LONGS_EQUAL(I2C_SUCCESS,I2cDriver_SendData(ZERO));
  LONGS_EQUAL(LOW, scl);
  LONGS_EQUAL(LOW, sda);
  mock().checkExpectations();
}

TEST(MockI2cHw, SendData_One)
{
  scl = LOW;
  mock().expectOneCall("I2cHw_CheckLineState").withParameter("p1",SCL).andReturnValue(scl);
  mock().expectOneCall("I2cHw_ReleaseI2cLine").withParameter("p1",SDA);
  LONGS_EQUAL(I2C_SUCCESS,I2cDriver_SendData(ONE));
  LONGS_EQUAL(LOW, scl);
  LONGS_EQUAL(HIGH, sda);
  mock().checkExpectations();
}

// This is incorrect behavior
// TEST(MockI2cHw, SendByte_FailsIfSdaHigh)
// {
//   sda = HIGH;
//   mock().expectOneCall("I2cHw_CheckLineState")
//     .withParameter("p1",SDA)
//     .andReturnValue(sda);
//   LONGS_EQUAL(I2C_ERR_INVALID_LINE_STATE,I2cDriver_SendByte(0b10));
//   LONGS_EQUAL(HIGH, sda);
//   mock().checkExpectations();
// }
