extern "C"
{
  // #includes with C linkage
}

// #include with C++ linkage
#include "CppUTest/TestHarness.h"
#include "SampleTestGroup.h"

TEST_GROUP(SampleTestGroup)
{
  // Define data accessible to test group members here.

  void setup()
  {
    // initialization steps are executed before each TEST
  }
  void teardown()
  {
    // clean up steps are executed after each TEST
  }
};

// TEST(SampleTestGroup, WiringTest)
// {
//   FAIL("SampleTestGroup wired properly!");
// }

TEST(SampleTestGroup, SampleTest)
{
  STRCMP_EQUAL("hello", "hello");
  LONGS_EQUAL(1, 1);
  CHECK(true);
}
