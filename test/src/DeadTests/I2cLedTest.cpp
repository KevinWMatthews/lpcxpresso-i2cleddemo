extern "C"
{
  // #includes with C linkage
  #include "I2cLed.h"
  #include "I2cLedSpy.h"
}

// #include with C++ linkage
#include "CppUTest/TestHarness.h"
#include "I2cLedTest.h"


TEST_GROUP(I2cLedTest)
{
  // Define data accessible to test group members here.

  void setup()
  {
    I2cLed_Init();
  }
  void teardown()
  {
    // clean up steps are executed after each TEST
  }
};

TEST(I2cLedTest, LinesHighAfterInit)
{
  LONGS_EQUAL(HIGH, I2cLed_SclState());
  LONGS_EQUAL(HIGH, I2cLed_SdaState());
}

TEST(I2cLedTest, GenerateStartCondition)
{
  LONGS_EQUAL(HIGH, I2cLed_SclState());
  LONGS_EQUAL(HIGH, I2cLed_SdaState());
  I2cLed_Start();
  LONGS_EQUAL(LOW, I2cLed_SdaState());
  LONGS_EQUAL(HIGH, I2cLed_SclState());
}

TEST(I2cLedTest, GenerateStopCondition)
{
  I2cLedSpy_SetSda(LOW);

  LONGS_EQUAL(HIGH, I2cLed_SclState());
  LONGS_EQUAL(LOW, I2cLed_SdaState());
  I2cLed_Stop();
  LONGS_EQUAL(HIGH, I2cLed_SdaState());
  LONGS_EQUAL(HIGH, I2cLed_SclState());
}
