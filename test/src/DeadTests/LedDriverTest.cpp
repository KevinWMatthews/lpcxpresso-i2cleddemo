extern "C"
{
  // #includes with C linkage
  #include "LedDriver.h"
  #include "LedDriverSpy.h"
  #include "DataTypes.h"
}

// #include with C++ linkage
#include "CppUTest/TestHarness.h"
#include "LedDriverTest.h"

TEST_GROUP(LedDriverTest)
{
  void setup()
  {
    LedDriver_Init();
  }
  void teardown()
  {
  }
};

TEST(LedDriverTest, LedsOffAfterInit)
{
  LONGS_EQUAL(0, LedDriverSpy_AllLeds());
}

TEST(LedDriverTest, TurnLedOn)
{
  LedDriver_On(LED1);
  LONGS_EQUAL(TRUE, LedDriver_IsOn(LED1));
}

TEST(LedDriverTest, TurnLedOff)
{
  LedDriver_On(LED1);
  LedDriver_Off(LED1);
  LONGS_EQUAL(FALSE, LedDriver_IsOn(LED1));
}

TEST(LedDriverTest, PatternMatch)
{
  LedDriver_On(LED1);
  LedDriver_On(LED4);
  LONGS_EQUAL(TRUE, LedDriver_IsOn(LED1));
  LONGS_EQUAL(TRUE, LedDriver_IsOn(LED4));
  LONGS_EQUAL(0b1001, LedDriverSpy_AllLeds());
}

TEST(LedDriverTest, TurnAllLedsOn)
{
  LedDriver_AllOn();
  LONGS_EQUAL(ALL_ON, LedDriverSpy_AllLeds());
}

TEST(LedDriverTest, TurnAllLedsOff)
{
  LedDriver_AllOn();
  LedDriver_AllOff();
  LONGS_EQUAL(ALL_OFF, LedDriverSpy_AllLeds());
}

TEST(LedDriverTest, Invert)
{
  LedDriver_On(LED1);
  LedDriver_On(LED4);
  LedDriver_Invert();
  LONGS_EQUAL(0b0110, LedDriverSpy_AllLeds());
}
