#include "I2cLedSpy.h"
#include "LedDriver.h"

void I2cLedSpy_SetSda(I2cLineState state)
{
  if ( state == HIGH )
  {
    LedDriver_On(LED1);
  }
  else
  {
    LedDriver_Off(LED1);
  }
}

void I2cLedSpy_SetScl(I2cLineState state)
{
  if ( state == HIGH )
  {
    LedDriver_On(LED2);
  }
  else
  {
    LedDriver_Off(LED2);
  }
}
