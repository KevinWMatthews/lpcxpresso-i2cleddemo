#include "LedDriverSpy.h"

// Virtual LED state is stored as the lowest bits of a bitmask
u08 virtualLeds;

static s08 convertDriverLedToBitmask(LedNum ledNum);

//*** Fake implementations of hardware ***//
void LedDriver_Init(void)
{
  virtualLeds = 0;
}

void LedDriver_On(LedNum ledNum)
{
  virtualLeds |= convertDriverLedToBitmask(ledNum);
}

void LedDriver_Off(LedNum ledNum)
{
  virtualLeds &= ~(convertDriverLedToBitmask(ledNum));
}

void LedDriver_AllOn(void)
{
  virtualLeds |= ALL_ON;
}

void LedDriver_AllOff(void)
{
  virtualLeds &= ALL_OFF;
}

BOOL LedDriver_IsOn(LedNum ledNum)
{
  return (virtualLeds & convertDriverLedToBitmask(ledNum) ? TRUE : FALSE);
}

void LedDriver_Invert(void)
{
  // & with ALL_ON to ensure that we only affect the bits that we actually use
  virtualLeds = (~virtualLeds & ALL_ON);
}

//*** Implementation of spy functions ***//
// These functions report the status of the virtual hardware to the test code
BOOL LedDriverSpy_PatternMatch(u08 ledPattern)
{
  return (ledPattern == virtualLeds);
}

u08 LedDriverSpy_AllLeds(void)
{
  return virtualLeds;
}

// Internal functions
static s08 convertDriverLedToBitmask(LedNum ledNum)
{
  switch (ledNum)
  {
  case LED1:
    return 0x01;
  case LED2:
    return 0x02;
  case LED3:
    return 0x04;
  case LED4:
    return 0x08;
  default:
    return -1;
  }
  return -1;
}
