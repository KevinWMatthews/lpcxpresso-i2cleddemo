// Commented out to avoid multiple definitions of I2cHw functions

// #include "I2cHw.h"
// #include "LedDriver.h"

// //*** Internal function prototypes ***//
// static LedNum i2cLineToLed(I2cLine line);

// //*** Public functions ***//
// void I2cHw_Init(void)
// {
//   LedDriver_Init();
// }

// I2cLineState I2cHw_CheckLineState(I2cLine line)
// {
//   //TODO
//     if ( LedDriver_IsOn(LED2) == TRUE )
//     {
//       return HIGH;
//     }
//     else
//     {
//       return LOW;
//     }

//     if ( LedDriver_IsOn(LED1) == TRUE )
//     {
//       return HIGH;
//     }
//     else
//     {
//       return LOW;
//     }
// }

// void I2cHw_PullI2cLineLow(I2cLine line)
// {
//   LedNum led = i2cLineToLed(line);

//   LedDriver_Off(led);
// }

// void I2cHw_ReleaseI2cLine(I2cLine line)
// {
//   LedNum led = i2cLineToLed(line);

//   LedDriver_On(led);
// }

// //*** Internal functions ***//
// // Instead of typecasting
// static LedNum i2cLineToLed(I2cLine line)
// {
//   switch (line)
//   {
//   case SDA:
//     return LED1;
//   case SCL:
//     return LED2;
//   }
// }
