#include "I2cDriver.h"
#include "I2cHw.h"

s08 I2cDriver_ClockStart(void)
{
  if (I2cHw_CheckLineState(SCL) != HIGH)
    return I2C_ERR_INVALID_LINE_STATE;

  I2cHw_PullI2cLineLow(SCL);
  return I2C_SUCCESS;
}

void I2cDriver_Init(void)
{
  I2cHw_ReleaseI2cLine(SDA);
  I2cHw_ReleaseI2cLine(SCL);
}

s08 I2cDriver_SendByte(int byte)
{
}

s08 I2cDriver_SendData(I2cData data)
{
  if (I2cHw_CheckLineState(SCL) != LOW)
    return I2C_ERR_INVALID_LINE_STATE;

  if (data == ONE)
    I2cHw_ReleaseI2cLine(SDA);
  else
    I2cHw_PullI2cLineLow(SDA);

  return I2C_SUCCESS;
}

s08 I2cDriver_Start(void)
{
  if (I2cHw_CheckLineState(SCL) != HIGH)
    return I2C_ERR_INVALID_LINE_STATE;

  if (I2cHw_CheckLineState(SDA) != HIGH)
    return I2C_ERR_INVALID_LINE_STATE;

  I2cHw_PullI2cLineLow(SDA);
  return I2C_SUCCESS;
}

s08 I2cDriver_Stop(void)
{
  if (I2cHw_CheckLineState(SCL) != HIGH)
    return I2C_ERR_INVALID_LINE_STATE;

  if (I2cHw_CheckLineState(SDA) != LOW)
    return I2C_ERR_INVALID_LINE_STATE;

  I2cHw_ReleaseI2cLine(SDA);
  return I2C_SUCCESS;
}
