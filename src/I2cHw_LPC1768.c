#include "I2cHw.h"

void I2cHw_Init(void)
{
  //TODO initialize I2c hardware lines
}

void I2cHw_PullI2cLineLow(I2cLine line)
{
  //TODO pull I2C hardware line low
}

void I2cHw_ReleaseI2cLine(I2cLine line)
{
  //TODO release I2C hardware line (let if float high)
}

I2cLineState I2cHw_CheckLineState(I2cLine line)
{
  //TODO check the state of the I2C hardware line
}
